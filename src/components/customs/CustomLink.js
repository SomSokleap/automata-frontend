import React from "react";
import { Link } from "react-router-dom";

function CustomLink({ children, to, ...rest }) {
  return (
    <Link to={to} style={{ textDecoration: "none" }} {...rest}>
      {children}
    </Link>
  );
}

export default CustomLink;
