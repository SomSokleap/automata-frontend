import * as React from "react";
import Box from "@mui/material/Box";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";

export default function CustomMenu({ label, content }) {
  return (
    <PopupState
      variant="popover"
      popupId="popup-menu"
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
    >
      {(popupState) => (
        <>
          <Box {...bindTrigger(popupState)}>{label}</Box>
          <Menu {...bindMenu(popupState)}>
            {content.map((item, index) => (
              <MenuItem
                key={index}
                onClick={() => {
                  item.onClick();
                  popupState.close();
                }}
              >
                {item.label}
              </MenuItem>
            ))}
          </Menu>
        </>
      )}
    </PopupState>
  );
}
