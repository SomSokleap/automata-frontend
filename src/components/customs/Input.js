import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Slider,
  Stack,
  TextField,
  Typography,
  Box,
  Autocomplete,
  Collapse,
  Button,
  Popover,
} from "@mui/material";
// import { QuillEditor } from "components/editor";
// import { UploadMultiFile, UploadSingleFile } from "components/upload";
import { FastField, useFormikContext } from "formik";
import { capitalize, differenceWith, isArray, isEqual } from "lodash";
import React, { useState, useEffect, useRef } from "react";
import MapPicker from "react-google-map-picker";
import mapboxgl from "mapbox-gl";
import { capitalCaseTransform } from "change-case";
import { decode } from "html-entities";
import { DatePicker, DateTimePicker } from "@mui/lab";
import { TwitterPicker } from "react-color";

const styles = {
  FInputContainer: {
    width: "100%",
    padding: "16px",
    marginBottom: "16px",
  },
};

export const FInputContainer = ({
  title,
  children,
  inputSpacing,
  actions,
  ...other
}) => {
  return (
    <Stack
      elevation={2}
      component={Paper}
      style={styles.FInputContainer}
      {...other}
      spacing={2}
    >
      {title && (
        <Stack
          direction="row"
          alignItems="center"
          sx={{ width: "100%", justifyContent: "space-between" }}
        >
          <Typography variant="subtitle1" gutterBottom>
            {title}
          </Typography>
          {actions && (
            <Stack
              direction="row"
              justifyCotent="space-between"
              alignItems="center"
              spacing={2}
            >
              {actions}
            </Stack>
          )}
        </Stack>
      )}
      <Stack direction="column" spacing={inputSpacing || 2}>
        {children}
      </Stack>
    </Stack>
  );
};

export const FInput = ({ name, label, ...other }) => {
  return (
    <FastField name={name}>
      {({ field, meta }) => (
        <TextField
          fullWidth
          size="small"
          label={capitalize(label || name)}
          error={!!meta.error}
          helperText={meta.error}
          {...field}
          {...other}
        />
      )}
    </FastField>
  );
};

export const FSelect = ({ name, label, options, ...other }) => {
  return (
    <FastField name={name}>
      {({ field, meta }) => (
        <FormControl fullWidth size="small" error={!!meta.error}>
          <InputLabel id={`${name}-${label}`}>
            {options
              ? options.length > 0
                ? `${capitalize(label || name)}`
                : "No Options Available"
              : "---"}
          </InputLabel>
          <Select
            disabled={!options || options.length === 0}
            labelId={`${name}-${label}`}
            id={`${name}-${label}-select`}
            label={
              options
                ? options.length > 0
                  ? `${capitalize(label || name)}`
                  : "No Options Available"
                : "---"
            }
            value={field.value}
            {...field}
            {...other}
          >
            {options &&
              options.length > 0 &&
              options.map((option) => (
                <MenuItem value={option.value}>{option.label}</MenuItem>
              ))}
          </Select>

          {!!meta.error && <FormHelperText>{meta.error}</FormHelperText>}
        </FormControl>
      )}
    </FastField>
  );
};

export const FDatePicker = ({ name, label, ...other }) => {
  return (
    <FastField name={name}>
      {({ field, meta, form }) => (
        <DatePicker
          label={capitalize(label || name)}
          views={["year", "month", "day"]}
          inputFormat="dd/MM/yyyy"
          openTo="year"
          value={field.value}
          onChange={(newValue) => {
            form.setFieldValue(name, newValue);
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              error={!!meta.error}
              helperText={meta.error}
              fullWidth
              size="small"
            />
          )}
          {...other}
        />
      )}
    </FastField>
  );
};
export const FDateTimePicker = ({ name, label, ...other }) => {
  return (
    <FastField name={name}>
      {({ field, meta, form }) => (
        <DateTimePicker
          label={capitalize(label || name)}
          renderInput={(params) => (
            <TextField
              {...params}
              error={!!meta.error}
              helperText={meta.error}
              fullWidth
              size="small"
            />
          )}
          // openTo="year"
          value={field.value}
          onChange={(newValue) => {
            form.setFieldValue(name, newValue);
          }}
          {...other}
        />
      )}
    </FastField>
  );
};

export const FMultiSelect = ({ name, label, options }) => {
  return (
    <FastField name={name}>
      {({ field, meta }) => (
        <FormControl fullWidth size="small" error={!!meta.error}>
          <InputLabel id={`${name}-${label}`}>
            {options
              ? options.length > 0
                ? `${capitalize(label || name)}`
                : "No Options Available"
              : "---"}
          </InputLabel>
          <Select
            multiple
            disabled={!options || options.length === 0}
            labelId={`${name}-${label}`}
            id={`${name}-${label}-select`}
            label={
              options
                ? options.length > 0
                  ? `${capitalize(label || name)}`
                  : "No Options Available"
                : "---"
            }
            value={field.value}
            {...field}
          >
            {options &&
              options.length > 0 &&
              options.map((option) => (
                <MenuItem value={option.value}>{option.label}</MenuItem>
              ))}
          </Select>

          {!!meta.error && <FormHelperText>{meta.error}</FormHelperText>}
        </FormControl>
      )}
    </FastField>
  );
};

export const FAutoComplete = ({ name, label, options, ...other }) => {
  const { values } = useFormikContext();

  return (
    <FastField name={name}>
      {({ field, meta, form }) => (
        <Autocomplete
          size="small"
          fullWidth
          id={`${name}-${label}`}
          options={options}
          getOptionLabel={(option) => option.label}
          defaultValue={field.value}
          onChange={(event, value) => {
            form.setFieldValue(name, value);
          }}
          renderOption={(props, option, { selected }) => (
            <li {...props}>
              <Checkbox
                checked={values[name] && option.value === values[name].value}
              />
              {decode(option.label)}
            </li>
          )}
          renderInput={(params) => (
            <TextField
              {...params}
              label={capitalize(label || name)}
              placeholder={capitalize(label || name)}
            />
          )}
          {...other}
        />
      )}
    </FastField>
  );
};

export const FMultiSelectAutoComplete = ({
  name,
  label,
  options,
  ...other
}) => {
  const { values } = useFormikContext();
  const defaultValue = values[name].map((x) => {
    const index = options.findIndex((y) => y.value === x.value);
    if (index >= 0) {
      return options[index];
    }
  });

  return (
    <FastField name={name}>
      {({ field, meta, form }) => (
        <Autocomplete
          size="small"
          fullWidth
          multiple
          id={`${name}-${label}`}
          options={options}
          getOptionLabel={(option) => option.label}
          defaultValue={defaultValue}
          disableCloseOnSelect
          onChange={(event, value) => {
            form.setFieldValue(name, value);
          }}
          renderOption={(props, option, { selected }) => (
            <li {...props}>
              <Checkbox checked={selected} />
              {decode(option.label)}
            </li>
          )}
          renderInput={(params) => (
            <TextField
              {...params}
              label={capitalize(label || name)}
              placeholder={capitalize(label || name)}
              error={!!meta.error}
              helperText={meta.error}
            />
          )}
          {...other}
        />
      )}
    </FastField>
  );
};

export const FCheckBox = ({ name, label, value }) => {
  return (
    <FastField name={name}>
      {({ field, meta, form }) => (
        <FormControlLabel
          label={label}
          control={
            <Checkbox
              defaultChecked={field.value.includes(parseInt(value))}
              onChange={(e, checked) => {
                if (checked) {
                  form.setFieldValue(name, [...field.value, value]);
                } else {
                  form.setFieldValue(
                    name,
                    field.value.filter((x) => x !== value)
                  );
                }
              }}
            />
          }
          {...field}
          value={value}
        />
      )}
    </FastField>
  );
};

export const FColorPicker = ({ name, label, ...other }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const { errors, values } = useFormikContext();
  return (
    <Stack sx={{ width: "100%" }}>
      <Stack
        direction="row"
        onClick={handleClick}
        sx={{
          border: "1px solid #d4dadf",
          color: "#9ca8b3",
          p: "8.5px 14px",
          borderRadius: 1,
          height: "40px",
          alignItems: "center",
          justifyContent: "space-between",
          cursor: "pointer",
        }}
      >
        <Typography
          variant={"body2"}
          sx={{
            p: "4px 0 5px",
            fontSize: "1rem",
            lineHeight: "1.4375em",
            fontWeight: "400",
            color: "black",
          }}
        >
          {capitalize(label || name)}
        </Typography>
        <Box
          sx={{
            height: "25px",
            width: "25px",
            borderRadius: "25px",
            backgroundColor: values[name],
          }}
        />
      </Stack>
      <Popover
        id={name}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <FastField name={name}>
          {({ field, form }) => (
            <TwitterPicker
              styles={{
                width: "100%",
                height: "300px",
                backgroundColor: "red",
              }}
              color={field.value}
              onChangeComplete={(color) =>
                form.setFieldValue("color", color.hex)
              }
            />
          )}
        </FastField>
      </Popover>
      {errors[name] && (
        <Typography
          sx={{
            alignSelf: "flex-start",
            fontSize: "0.75rem",
            ml: 1,
            my: 1,
          }}
          color="error"
        >
          {errors[name]}
        </Typography>
      )}
    </Stack>
  );
};

// export const FQuill = ({ name, label }) => {
//   return (
//     <FastField name={name}>
//       {({ field, meta, form }) => (
//         <>
//           <QuillEditor
//             placeholder={
//               label
//                 ? `Write something about ${capitalize(label)}`
//                 : `Write something about ${capitalize(name)}...`
//             }
//             error={!!meta.error}
//             id={`${name}-quill`}
//             simple
//             value={decode(field.value)}
//             onChange={(value) => form.setFieldValue(name, value)}
//           />
//           {!!meta.error && (
//             <Typography
//               sx={{ color: "red", fontSize: "12px", m: "14px 8px 0" }}
//             >
//               {meta.error}
//             </Typography>
//           )}
//         </>
//       )}
//     </FastField>
//   );
// };

// export const FMap = ({ name, label }) => {
//   const [defaultLocation, setDefaultLocation] = React.useState({
//     lat: 11.5932959,
//     lng: 104.9087607,
//   });
//   const [location, setLocation] = React.useState(defaultLocation);
//   const [zoom, setZoom] = React.useState(13);

//   function handleChangeLocation(lat, lng) {
//     setLocation({ lat: lat, lng: lng });
//   }

//   function handleChangeZoom(newZoom) {
//     setZoom(newZoom);
//   }

//   return (
//     <FastField name={name}>
//       {({ field, meta, form }) => (
//         <Stack>
//           <MapPicker
//             defaultLocation={defaultLocation}
//             zoom={zoom}
//             style={{ height: "550px", borderRadius: "5px" }}
//             onChangeLocation={(lat, lng) =>
//               form.setFieldValue(name, { lat, lng })
//             }
//             onChangeZoom={handleChangeZoom}
//             apiKey="AIzaSyAkBhTU6Tc8FNdu64ZRG4rPm2bin7H7OOI"
//             {...field}
//           />
//           {!!meta.error && (
//             <Typography
//               sx={{ color: "red", fontSize: "12px", m: "14px 8px 0px" }}
//             >
//               {meta.error}
//             </Typography>
//           )}
//         </Stack>
//       )}
//     </FastField>
//   );
// };

export const FMap = ({ name, label }) => {
  const { values, setFieldValue } = useFormikContext();
  mapboxgl.accessToken =
    "pk.eyJ1Ijoic29tLXNva2xlYXAiLCJhIjoiY2t4ZnYxN3lnNWU2OTJ2cW9oNTI3MnprbSJ9.jqPxVhkPWDOMoy03M1iccA";

  const defaultLocation = {
    lat: 11.5932959,
    lng: 104.9087607,
  };

  const mapContainer = useRef(null);
  const map = useRef(null);
  const [lng, setLng] = useState(values[name].lng || defaultLocation.lng);
  const [lat, setLat] = useState(values[name].lat || defaultLocation.lat);
  const [zoom, setZoom] = useState(12);

  useEffect(() => {
    if (map.current) return; // initialize map only once
    map.current = new mapboxgl.Marker({
      color: "#FF0000",
      draggable: true,
    })
      .setLngLat([lng, lat])
      .addTo(
        new mapboxgl.Map({
          container: mapContainer.current,
          style: "mapbox://styles/mapbox/streets-v11",
          center: [lng, lat],
          zoom: zoom,
        })
      )
      .on("dragend", ({ type, target }) => {
        const lngLat = target._lngLat;
        setFieldValue(name, { lng: lngLat.lng, lat: lngLat.lat });
        setTimeout(() => {
          console.log(name, values.name);
        }, 500);
      });
  });

  return (
    <FastField name={name}>
      {({ meta }) => (
        <Stack>
          <Stack
            sx={{
              "& .map-container": { height: "484px" },
            }}
          >
            <div ref={mapContainer} className="map-container" />
          </Stack>
          {!!meta.error && (
            <Typography
              sx={{ color: "red", fontSize: "12px", m: "14px 8px 0px" }}
            >
              {meta.error}
            </Typography>
          )}
        </Stack>
      )}
    </FastField>
  );
};

function valuetext(value) {
  return `${value}%`;
}

export const FSlide = ({ name, label, max, type }) => {
  return (
    <FastField name={name}>
      {({ field, meta }) => (
        <Stack direction="column" sx={{ width: "100%" }}>
          <Stack direction="row" justifyContent="space-between">
            <Typography>{capitalize(label || name)}</Typography>
            <Typography>
              {field.value}
              {type && type}
            </Typography>
          </Stack>
          <Slider
            size="small"
            getAriaValueText={valuetext}
            step={5}
            max={max}
            {...field}
            valueLabelDisplay="auto"
          />

          {!!meta.error && (
            <Typography
              sx={{ color: "red", fontSize: "12px", m: "14px 8px 0px" }}
            >
              {meta.error}
            </Typography>
          )}
        </Stack>
      )}
    </FastField>
  );
};

// export const FUploadSingleFile = ({ name, label, uploadProp }) => {
//   return (
//     <FastField name={name}>
//       {({ field, meta, form }) => (
//         <Box sx={{ mb: 2 }}>
//           {label && (
//             <Typography sx={{ fontSize: "17px", mb: 1 }}>
//               {capitalize(label)}
//             </Typography>
//           )}
//           <UploadSingleFile
//             error={!!meta.error}
//             file={field.value}
//             onDrop={(acceptedFiles) => {
//               const file = acceptedFiles[0];
//               if (file) {
//                 file.preview = URL.createObjectURL(file);
//                 form.setFieldValue(name, file);
//                 console.log("File", file);
//               }
//             }}
//             {...uploadProp}
//           />
//           {!!meta.error && (
//             <Typography color={"error"} sx={{ fontSize: "12px", m: 1 }}>
//               {meta.error}
//             </Typography>
//           )}
//         </Box>
//       )}
//     </FastField>
//   );
// };

// export const FUploadMultiFile = ({ name, label, uploadProp }) => {
//   return (
//     <FastField name={name}>
//       {({ field, meta, form }) => (
//         <Box sx={{ mb: 2 }}>
//           {label && (
//             <Typography sx={{ fontSize: "17px", mb: 1 }}>
//               {capitalize(label)}
//             </Typography>
//           )}
//           <UploadMultiFile
//             hideUploadButton
//             onRemoveAll={() => form.setFieldValue(name, [])}
//             onRemove={(image) =>
//               form.setFieldValue(
//                 name,
//                 differenceWith(field.value, [image], isEqual)
//               )
//             }
//             showPreview={true}
//             error={!!meta.error}
//             files={field.value}
//             onDrop={(value, e) =>
//               form.setFieldValue(name, [...field.value, ...value])
//             }
//             {...uploadProp}
//           />
//           {!!meta.error && (
//             <Typography color={"error"} sx={{ fontSize: "12px", m: 1 }}>
//               {meta.error}
//             </Typography>
//           )}
//         </Box>
//       )}
//     </FastField>
//   );
// };

// export const FMultiSelectTreeAutoComplete = ({ name, label, options }) => {
//   const [currentOpions, setcurrentOpions] = React.useState(options);

//   const onChange = (currentNode, selectedNodes) => {
//     console.log("onChange::", currentNode, selectedNodes);
//     const selectNodeParentId = currentNode.parent_id
//     let updatedOptions = currentOpions
//     updatedOptions.forEact(option=>{
//       if(option.id === selectNodeParentId){

//       }else{

//       }
//     })
//   };
//   const onAction = (node, action) => {
//     console.log("onAction::", action, node);
//   };
//   const onNodeToggle = (currentNode) => {
//     console.log("onNodeToggle::", currentNode);
//   };

//   return (
//     <FastField name={name}>
//       {({ field, meta }) => (
//         <MultiSelectAutoComplete
//           options={currentOpions}
//           onChange={onChange}
//           onAction={onAction}
//           onNodeToggle={onNodeToggle}
//           placeholder={capitalize(label || name)}
//           label={capitalize(label || name)}
//         />
//       )}
//     </FastField>
//   );
// };
