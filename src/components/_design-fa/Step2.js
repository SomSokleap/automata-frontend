import { FInput, FInputContainer } from "../customs/Input";
import { Add, Remove, RemoveCircleRounded } from "@mui/icons-material";
import {
  Button,
  FormControlLabel,
  IconButton,
  Stack,
  Switch,
  ToggleButton,
  Tooltip,
} from "@mui/material";
import { useFormikContext, FieldArray } from "formik";
import { AnimatePresence, motion } from "framer-motion";
import { uniqueId } from "lodash";
import React, { useEffect } from "react";
import { varFadeInRight } from "../animate";

export default function Step2({ setAllowNextStep }) {
  const { values, setFieldValue } = useFormikContext();
  const initState = {
    name: "",
    uid: uniqueId(),
    is_final: false,
    is_start: false,
    is_dead: false,
  };

  useEffect(() => {
    if (values.states.length > 0) {
      let ok = 0;
      let startStateCount = 0;
      let finalStateCount = 0;

      values.states.forEach((state) => {
        if (state.name.length > 0) {
          ok++;
        }
        if (state.is_start) {
          startStateCount++;
        }
        if (state.is_final) {
          finalStateCount++;
        }
      });
      if (
        ok === values.states.length &&
        startStateCount == 1 &&
        finalStateCount >= 1
      ) {
        const initTrans = [];
        let index = 0;
        for (const state of values.states) {
          for (const symbol of values.symbols.split(",")) {
            const tran = {
              from: state.name,
              to: [],
              input: symbol,
            };
            initTrans.push(tran);
            setFieldValue(`transitions[${index}]`, tran);
            index++;
          }
        }
        console.log("initTrans", initTrans);

        setFieldValue("transitions_json", JSON.stringify(initTrans));

        console.log("values.transitions", values.transitions);
        setAllowNextStep(true);
      } else {
        setFieldValue("transitions", []);
        setAllowNextStep(false);
      }
    } else {
      setFieldValue("transitions", []);
      setAllowNextStep(false);
    }
  }, [values.states]);

  return (
    <FieldArray name="states">
      {({ push, remove }) => (
        <Stack spacing={2}>
          <Stack direction={"row"} justifyContent={"flex-end"}>
            <Button
              variant="contained"
              onClick={() => push(initState)}
              startIcon={<Add />}
            >
              Add State
            </Button>
          </Stack>
          <AnimatePresence>
            {values.states.map((state, index) => (
              <Stack key={state.uid} component={motion.div} {...varFadeInRight}>
                <FInputContainer>
                  <FInput label="State Name" name={`states[${index}].name`} />
                  <Stack direction={"row"} flexWrap={"wrap"}>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={values.states[index].is_start}
                          onChange={(e, checked) =>
                            setFieldValue(`states[${index}].is_start`, checked)
                          }
                          inputProps={{
                            "aria-label": "controlled",
                          }}
                        />
                      }
                      label="Is Start"
                    />
                    <FormControlLabel
                      control={
                        <Switch
                          checked={values.states[index].is_final}
                          onChange={(e, checked) =>
                            setFieldValue(`states[${index}].is_final`, checked)
                          }
                          inputProps={{
                            "aria-label": "controlled",
                          }}
                        />
                      }
                      label="Is Final"
                    />
                    <FormControlLabel
                      control={
                        <Switch
                          checked={values.states[index].is_dead}
                          onChange={(e, checked) =>
                            setFieldValue(`states[${index}].is_dead`, checked)
                          }
                          inputProps={{
                            "aria-label": "controlled",
                          }}
                        />
                      }
                      label="Is Dead"
                    />
                  </Stack>
                  <Stack direction={"row"} justifyContent="flex-end">
                    <Tooltip title="Remove State">
                      <Button
                        onClick={() => remove(index)}
                        color="error"
                        startIcon={<RemoveCircleRounded />}
                      >
                        Remove
                      </Button>
                    </Tooltip>
                  </Stack>
                </FInputContainer>
              </Stack>
            ))}
          </AnimatePresence>
        </Stack>
      )}
    </FieldArray>
  );
}
