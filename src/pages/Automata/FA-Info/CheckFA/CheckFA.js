import axios from "axios";
import { Paper, Stack, Typography, Box } from "@mui/material";
import React, { useState, useEffect } from "react";
import { useMainContext } from "src/contexts/MainContext";
import { toast } from "react-toastify";
import LoadingScreen from "src/components/LoadingScreen";
import { apiUrl } from "src/config";

export default function CheckFA() {
  const { currentSelected } = useMainContext();
  const [message, setMessage] = useState();
  const [loading, setLoading] = useState(true);

  const checkFA = async () => {
    try {
      console.log("Call API");
      const res = await axios.post(apiUrl + "/api/CheackFA", {
        fa_id: currentSelected.fa_id,
      });
      console.log("Get result");
      setMessage(res.data.message);
      setLoading(false);
      console.log("Set Loading false");
    } catch (e) {
      toast.error(e.message);
    }
  };

  useEffect(() => {
    checkFA();
  }, []);

  return (
    <Stack sx={{ mt: 2 }}>
      <Paper sx={{ p: 2 }} elevation={6}>
        {!loading ? (
          <Stack direction="row" spacing={2}>
            <Typography sx={{ fontWeight: "700" }}>
              This is Finit Automaton is a : {""}
            </Typography>
            <Typography>{message}</Typography>
          </Stack>
        ) : (
          <Box sx={{ height: 500, width: "100%" }}>
            <LoadingScreen />
          </Box>
        )}
      </Paper>
    </Stack>
  );
}
