import axios from "axios";
import { createContext, useContext, useState, useEffect } from "react";
import { apiUrl } from "src/config";
import customSnackbar from "src/utils/snackbar";

const MainContext = createContext();

const useMainContext = () => useContext(MainContext);

const MainContextProvider = ({ children }) => {
  const [currentSelected, setCurrentSelected] = useState();
  const [automatas, setAutomatas] = useState();
  const [isCreating, setIsCreating] = useState(false);

  const initAutomata = async () => {
    try {
      const res = await axios.get(apiUrl + "/api/getAllDFA");

      const body = res.data.data;

      setAutomatas(body);
      console.log("initAutomata", body);
    } catch (error) {
      customSnackbar.error(error.message);
    }
  };

  const checkFA = async () => {
    try {
      console.log("fa_id: ", currentSelected.id);
      const res = await axios.post(apiUrl + "/api/CheackFA", {
        fa_id: currentSelected.id,
      });
      console.log("Check FA Result: ", res.data);
      // const _isDFA = res.data.message.include("DFA");
      setCurrentSelected({ ...currentSelected, isDFA: res.data.message });
    } catch (e) {
      toast.error(e.message);
    }
  };

  const DesginFAHandler = async (values) => {
    try {
      const body = {
        ...values,
      };
      const res = await axios.post("/api/DesignDFA", body);
      const createdAutomata = res.data;

      await initAutomata();
      setIsCreating(false);
      customSnackbar.success("Design FA is successfully");
    } catch (error) {
      customSnackbar.error(error.message);
    }
  };

  const AcceptString = async (values) => {
    try {
      const body = {
        ...values,
      };
      const res = await axios.post("/api/acceptString", body);
      const createdAutomata = res.data;

      await initAutomata();
      setIsCreating(false);
      customSnackbar.success("Design FA is successfully");
    } catch (error) {
      customSnackbar.error(error.message);
    }
  };

  // useEffect(() => {
  //   if (currentSelected) {
  //     checkFA();
  //   }
  // }, [currentSelected]);

  useEffect(() => {
    initAutomata();
    return () => {
      setAutomatas(undefined);
    };
  }, []);

  const contextValue = {
    initAutomata,
    automatas,
    setAutomatas,
    isCreating,
    currentSelected,
    setCurrentSelected,
    setIsCreating,
    DesginFAHandler,
    checkFA,
    AcceptString,
  };
  return (
    <MainContext.Provider value={contextValue}>{children}</MainContext.Provider>
  );
};

export { MainContext, useMainContext, MainContextProvider };
