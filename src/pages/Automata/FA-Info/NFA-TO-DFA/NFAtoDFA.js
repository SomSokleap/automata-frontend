import { LoadingButton } from "@mui/lab";
import SettingsIcon from "@mui/icons-material/Settings";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";

export default function NFAtoDFA() {
  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <Stack sx={{ mt: 2 }}>
      <Paper sx={{ p: 2 }} elevation={6}>
        <Stack>
          <Typography sx={{ fontWeight: "700", mb: 2 }}>
            Convert the Nondeterministic Finite Automaton (NFA) to Deterministic
            Finite Automaton (DFA)
          </Typography>
          <FormControl sx={{ width: "100%" }} size="small">
            <InputLabel id="demo-select-small">Age</InputLabel>
            <Select
              fullWidth
              labelId="demo-select-small"
              id="demo-select-small"
              value={age}
              label="Age"
              onChange={handleChange}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </FormControl>
        </Stack>
        <Stack
          sx={{ mt: 2 }}
          direction="row"
          justifyContent="flex-end"
          alignItems={"center"}
        >
          <LoadingButton
            sx={{
              backgroundColor: "primary.main",
              "&:hover": {
                backgroundColor: "primary.light",
              },
            }}
            // type="submit"
            // loading={isSubmitting}
            startIcon={<SettingsIcon />}
            variant="contained"
          >
            Convert
          </LoadingButton>
        </Stack>
      </Paper>
    </Stack>
  );
}
