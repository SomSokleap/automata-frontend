import DropdownTreeSelect from "react-dropdown-tree-select";
import "react-dropdown-tree-select/dist/styles.css";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { decode } from "html-entities";
import _ from "lodash";

export const convertObject = (label, value, data) => {
  let currentLabel, currentValue;
  currentLabel = decode(_.get(data, label));
  currentValue = decode(_.get(data, value));
  return { ...data, label: currentLabel, value: currentValue, checked: false };
};

export const convertArrayObject = (label, value, data) => {
  let convertedData = [];

  data.forEach((item) => {
    const convertedItem = convertObject(label, value, item);
    if (convertedItem.children && convertedItem.children.length > 0) {
      convertedItem.children = convertArrayObject(
        label,
        value,
        convertedItem.children
      );
    }
    convertedData.push(convertedItem);
  });

  return convertedData;
};

const MultiSelectAutoComplete = ({
  onChange,
  onAction,
  onNodeToggle,
  options,
  error,
  placeholder,
  label,
  value,
}) => {
  const style = {
    width: "100%",
    "& .react-dropdown-tree-select .dropdown .dropdown-trigger": {
      zIndex: 100,
      borderRadius: "8px",
      lineHeight: "20px",
      minHeight: "40px",
      maxHeight: "200px",
      display: "inline-block",
      overflow: "auto",
      border: "1px solid #b9b9b9",
    },
    "& .tag-list": {
      display: "inline",
      padding: "0",
      margin: "0",
      width: "100%",
    },
    "& .tag-item": {
      display: "inline-block",
      margin: "4px",
      //   width: "85%",
    },
    "& .toggle.collapsed:after": {
      fontFamily: "'Material Icons'",
      content: '"add_circle"',
      fontStyle: "normal",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    "& .toggle.expanded:after": {
      fontFamily: "'Material Icons'",
      content: '"remove_circle_outlined"',
      fontStyle: "normal",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      margin: "0px -16px 0 0",
    },
    "& .react-dropdown-tree-select .dropdown .dropdown-content": {
      position: "absolute",
      padding: "8px",
      zIndex: "100",
      background: "#fff",
      borderTop: "1px solid rgba(0,0,0,.05)",
      boxShadow: "0 5px 8px rgba(0,0,0,.15)",
      borderRadius: "8px",
      padding: "8px",
    },
    "& .node": {
      listStyle: "none",
      whiteSpace: "nowrap",
      padding: "4px",
      display: "flex",
    },
    "& .checkbox-item, .radio-item": {
      verticalAlign: "middle",
      margin: "0 4px 0 0",
      cursor: "pointer",
    },
    "& .react-dropdown-tree-select .dropdown": {
      position: "relative",
      display: "table",
      width: "100%",
    },
    "& .react-dropdown-tree-select .dropdown .dropdown-trigger.arrow": {
      cursor: "pointer",
      width: "100%",
    },
    "& .react-dropdown-tree-select .dropdown .dropdown-trigger.arrow.top:after":
      {
        fontFamily: "'Material Icons'",
        content: '""',
        verticalAlign: "middle",
        color: "#3c3c3c",
        marginRight: "2px",
        fontSize: "25px",
      },
    "& .react-dropdown-tree-select .dropdown .dropdown-trigger.arrow.bottom:after":
      {
        fontFamily: "'Material Icons'",
        content: '""',
        verticalAlign: "middle",
        color: "#3c3c3c",
        marginRight: "2px",
        fontSize: "25px",
      },
    "& .tag-item .search": {
      padding: "0px 4px",
      border: "none",
      fontSize: "16px",
      width: "100%",
      outline: "none",
    },
    "& .tag": {
      backgroundColor: "#f4f4f4",
      border: "1px solid #e9e9e9",
      padding: "4px 8px",
      borderRadius: "8px",
      display: "inline-block",
    },
    "& .tag-remove": {
      color: "#a0a0a0",
      //   margin: "0px 0px 0px 3px",
      fontSize: "100%",
      lineHeight: "100%",
      cursor: "pointer",
      backgroundColor: "lightgray",
      border: "none",
      outline: "none",
      width: "20px",
      alignSelf: "center",
      height: "20px",
      borderRadius: "100px",
    },
  };
  return (
    <Stack sx={style}>
      <DropdownTreeSelect
        showPartiallySelected
        data={options}
        onChange={onChange}
        onAction={onAction}
        onNodeToggle={onNodeToggle}
        texts={{ placeholder, label }}
      />
      {!!error && (
        <Typography sx={{ color: "error", fontSize: "12px", m: "14px 8px 0" }}>
          {error}
        </Typography>
      )}
    </Stack>
  );
};

export default MultiSelectAutoComplete;
