import React, { useEffect } from "react";
import { useFormikContext } from "formik";

import { Stack } from "@mui/material";
import { FInput } from "../customs/Input";
import { validateSetFormat } from "src/utils/strings";

export default function Step1({ setAllowNextStep }) {
  const { values } = useFormikContext();

  useEffect(() => {
    const endWithComma = values.symbols[values.symbols.length - 1] === ",";

    if (
      values.symbols.length > 0 &&
      validateSetFormat(values.symbols) &&
      !endWithComma
    ) {
      setAllowNextStep(true);
    } else {
      setAllowNextStep(false);
    }
  }, [values.symbols]);

  return (
    <Stack>
      <FInput label="Symbols" name="symbols" />
    </Stack>
  );
}
