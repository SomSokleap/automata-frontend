import React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import CheckFA from "./CheckFA/CheckFA";
import AcceptString from "./AcceptString/AcceptString";
import NFAtoDFA from "./NFA-TO-DFA/NFAtoDFA";
import MinimizeDFA from "./MinimizeDFA/MinimizeDFA";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 1 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function FAInfo() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Box sx={{ boxShadow: 6 }}>
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons
          allowScrollButtonsMobile
          aria-label="scrollable force tabs example"
          sx={{ borderRight: 1, borderColor: "divider" }}
        >
          <Tab label="Check FA" {...a11yProps(0)} />
          <Tab label="Accept String" {...a11yProps(1)} />
        </Tabs>
      </Box>
      <Box>
        <TabPanel value={value} index={0}>
          <CheckFA />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <AcceptString />
        </TabPanel>
      </Box>
    </>
  );
}
