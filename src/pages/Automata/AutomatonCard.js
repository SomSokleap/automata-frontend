import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Stack,
  Typography,
  Box,
  Tab,
} from "@mui/material";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import React, { useState } from "react";
import { MotionInView, varFadeInLeft } from "src/components/animate";
import PopupWrapperModal from "src/components/PopupWrapperModal";
import FAInfo from "./FA-Info";
import { useMainContext } from "src/contexts/MainContext";

export default function AutomatonCard({ data }) {
  const [value, setValue] = React.useState("1");
  const { currentSelected, setCurrentSelected } = useMainContext();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // const [isOpening, setIsOpening] = useState();
  return (
    <>
      <Grid item xs={12} md={6} lg={4} xl={3}>
        <MotionInView variants={varFadeInLeft}>
          <Card>
            <CardActionArea onClick={() => setCurrentSelected(data)}>
              <CardContent>
                <CardMedia
                  component="img"
                  height="140"
                  image="https://www.tutorialspoint.com/automata_theory/images/dfa_graphical_representation.jpg"
                  alt=""
                />
              </CardContent>
              <Stack sx={{ p: 2 }}>
                <Typography>
                  An automaton (Automata in plural) is an abstract
                  self-propelled computing device which follows a predetermined
                  sequence of operations automatically. An automaton with a
                  finite number of states is called a Finite Automaton (FA) or
                  Finite-State Machine (FSM). The figure at right illustrates a
                  finite-state machine,
                </Typography>
              </Stack>
            </CardActionArea>
          </Card>
        </MotionInView>
      </Grid>
    </>
  );
}
