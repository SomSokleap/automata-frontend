import * as React from "react";
import Box from "@mui/material/Box";
import * as Yup from "yup";
import VerticalStepper from "../Stepper/VerticalStepper";
import { Form, Formik } from "formik";
import VerticalLinearStepper from "src/components/_design-fa/FormStepper";
import LoadingScreen from "src/components/LoadingScreen";

export default function Create() {
  const [loading, setLoading] = React.useState(true);

  const initialValues = {
    symbols: "",
    states: [
      {
        name: "",
        is_final: false,
        is_start: false,
        is_dead: false,
      },
    ],
    transitions: [],
    transitions_json: "",
  };

  const validationSchema = Yup.object().shape({});

  return (
    <Formik
      initialValues={initialValues}
      validateOnBlur={false}
      validateOnChange={false}
      onSubmit={() => {}}
      validationSchema={validationSchema}
    >
      {({ errors, isSubmitting }) => (
        <>
          <Form style={{ padding: "18px" }}>
            <VerticalLinearStepper />
          </Form>
        </>
      )}
    </Formik>
  );
}
