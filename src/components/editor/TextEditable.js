import { TextField, Typography } from "@mui/material";
import { isEmpty } from "lodash-es";
import React from "react";
import { LongPressDetectEvents, useLongPress } from "use-long-press";

function TextEditable({ label, onSubmit }) {
  const [toggle, setToggle] = React.useState(true);
  const [value, setValue] = React.useState(label);
  // eslint-disable-next-line
  const [enabled, setEnabled] = React.useState(true);
  const callback = React.useCallback(() => {
    setToggle(false);
  }, []);
  const bind = useLongPress(enabled ? callback : null, {
    threshold: 500,
    captureEvent: true,
    cancelOnMovement: false,
    detect: LongPressDetectEvents.BOTH,
  });

  const submitHandler = () => {
    if (isEmpty(value)) {
      return;
    }
    onSubmit(value);
    setToggle(true);
  };

  return (
    <>
      {toggle ? (
        <Typography
          onClick={() => {
            setToggle(false);
          }}
          {...bind}
        >
          {value}
        </Typography>
      ) : (
        <TextField
          multiline
          hiddenLabel
          placeholder="Empty"
          defaultValue="Small"
          variant="outlined"
          size="small"
          value={value}
          onChange={(e) => setValue(e.target.value)}
          InputProps={{ style: { maxWidth: "100%" } }}
          onKeyDown={(e) => {
            if (e.key === "Enter" || e.key === "Escape") {
              submitHandler();
              e.preventDefault();
              e.stopPropagation();
            }
          }}
          onBlur={submitHandler}
        />
      )}
    </>
  );
}

export default TextEditable;
