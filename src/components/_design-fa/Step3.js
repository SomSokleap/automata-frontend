import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { useFormikContext } from "formik";
import React from "react";
import { FMultiSelect } from "../customs/Input";

export default function Step3({ setAllowNextStep }) {
  const { values, setFieldValue } = useFormikContext();
  const trans = JSON.parse(values.transitions_json);

  const RenderColumnSelect = ({ state, symbol }) => {
    const transitionIndex = values.transitions.findIndex(
      (t) => t.from === state.name && t.input === symbol
    );
    if (transitionIndex > -1) {
      return (
        <FMultiSelect
          options={values.states.map((s) => ({
            label: s.name,
            value: s.name,
          }))}
          label="States"
          name={`transitions[${transitionIndex}].to`}
        />
      );
    }
    return <i>None</i>;
  };

  return (
    <TableContainer component={Paper}>
      <Table aria-label="fa table">
        <TableHead>
          <TableRow>
            <TableCell>States</TableCell>
            {values.symbols.split(",").map((symbol) => (
              <TableCell align="right">Input&nbsp;({symbol})</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {values.states.map((state) => (
            <TableRow
              key={state.name}
              sx={{
                "&:last-child td, &:last-child th": {
                  border: 0,
                },
              }}
            >
              <TableCell component="th" scope="row">
                {state.name}
              </TableCell>
              {values.symbols.split(",").map((symbol) => (
                <TableCell align="right">
                  <RenderColumnSelect state={state} symbol={symbol} />
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
