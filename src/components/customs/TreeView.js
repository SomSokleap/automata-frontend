import Collapse from "@mui/material/Collapse";
import PropTypes from "prop-types";
import * as React from "react";
// web.cjs is required for IE11 support
import { animated, useSpring } from "react-spring";
import DataTreeviewWithContextMenu from "../TreeViewWithContextMenu";

function TransitionComponent(props) {
  const style = useSpring({
    from: {
      opacity: 0,
      transform: "translate3d(20px,0,0)",
    },
    to: {
      opacity: props.in ? 1 : 0,
      transform: `translate3d(${props.in ? 0 : 20}px,0,0)`,
    },
  });

  return (
    <animated.div style={style}>
      <Collapse {...props} />
    </animated.div>
  );
}

TransitionComponent.propTypes = {
  /**
   * Show the component; triggers the enter or exit states
   */
  in: PropTypes.bool,
};

export default function CustomizedTreeView({ data }) {
  return (
    <DataTreeviewWithContextMenu
      data={data} // tree data
      defaultleftendnodeicon="true"
      selectedNode={{}} // selected node data
    />
  );
}
