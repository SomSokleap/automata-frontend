import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Typography } from '@mui/material';
import { Button, Grid, Stack } from '@mui/material';
import { Backup } from '@mui/icons-material';

export default function FormInput() {
  const [fa, setFA] = React.useState('');

  const handleChange = (event) => {
    setFA(event.target.value);
  };
  return (
    <Box
      component='form'
      sx={{
        '& .MuiTextField-root': { mt: 2 },
      }}
      noValidate
      autoComplete='off'
    >
      <Typography sx={{ pb: 2 }} variant='h6'>
        Check if the String is Accept of Reject by the Finit Automaton (FA)
      </Typography>
      <FormControl fullWidth>
        <InputLabel>Finite Automata (FA)</InputLabel>
        <Select value={fa} label='Finite Automata (FA)' onChange={handleChange}>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
      </FormControl>
      <TextField label='String' variant='outlined' fullWidth />
      <Stack sx={{ pt: 2 }} spacing={2} direction='row'>
        <Grid container justifyContent='flex-end'>
          <Button type='submit' variant='contained' startIcon={<Backup />}>
            Submit
          </Button>
        </Grid>
      </Stack>
    </Box>
  );
}
