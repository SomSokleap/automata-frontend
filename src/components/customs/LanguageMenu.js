import * as React from "react";
import Box from "@mui/material/Box";
import { ArrowDropDown } from "@mui/icons-material";
import { Stack } from "@mui/material";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import useLocales from "../../hooks/useLocales";

export default function AccountMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const { currentLang, onChangeLang } = useLocales();
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (lang) => {
    if (lang) {
      onChangeLang(lang);
    }
    setAnchorEl(null);
  };
  return (
    <>
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Tooltip title="Account settings">
          <Stack
            direction="row"
            sx={{ alignItems: "center", cursor: "pointer" }}
            onClick={handleClick}
          >
            <img
              height={20}
              width={39}
              src={currentLang.icon}
              alt="language"
              style={{ borderRadius: "5px" }}
            />
            <ArrowDropDown sx={{ color: "gray" }} />
          </Stack>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={() => handleClose(null)}
        onClick={() => handleClose(null)}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem onClick={() => handleClose("en")}>
          <Stack direction="row" sx={{ alignItems: "center" }}>
            <img
              height={20}
              width={39}
              src="/icons/flags/flag_en.png"
              alt="language"
              style={{ borderRadius: "5px" }}
            />
            <Typography sx={{ ml: 1 }}>English</Typography>
          </Stack>
        </MenuItem>
        <MenuItem onClick={() => handleClose("kh")}>
          <Stack direction="row" sx={{ alignItems: "center" }}>
            <img
              height={20}
              width={39}
              src="/icons/flags/flag_kh.png"
              alt="language"
              style={{ borderRadius: "5px" }}
            />
            <Typography sx={{ ml: 1 }}>ខ្មែរ</Typography>
          </Stack>
        </MenuItem>
        <MenuItem onClick={() => handleClose("ch")}>
          <Stack direction="row" sx={{ alignItems: "center" }}>
            <img
              height={20}
              width={39}
              src="/icons/flags/flag_ch.png"
              alt="language"
              style={{ borderRadius: "5px" }}
            />
            <Typography sx={{ ml: 1 }}>Chinese</Typography>
          </Stack>
        </MenuItem>
      </Menu>
    </>
  );
}
