import Autocomplete from "@mui/material/Autocomplete";
import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";
import * as React from "react";

export default function AsynchronousAutoComplete({
  staticOptions,
  label,
  onChange,
  value,
  placeholder,
  disabled,
  small,
  error,
  disablePortal,
}) {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  // eslint-disable-next-line
  const [currentSelect, setCurrentSelect] = React.useState(value);
  const loading = open && options.length === 0;

  React.useEffect(() => {
    // eslint-disable-next-line
    let active = true;
    if (!loading) {
      return undefined;
    }
    if (staticOptions) {
      return setOptions([...staticOptions]);
    }

    setCurrentSelect(value);
    return () => {
      active = false;
    };
    // eslint-disable-next-line
  }, [loading]);

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  return (
    <Autocomplete
      disablePortal={!!disablePortal}
      size={small ? "small" : "medium"}
      disabled={disabled}
      id={label}
      sx={{ flex: 1 }}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      value={value}
      onChange={(event, data) => {
        setCurrentSelect(data);
        if (data && data.value) {
          onChange(data);
        } else {
          onChange(null);
        }
      }}
      isOptionEqualToValue={(option, value) => option.value === value}
      autoHighlight
      options={options}
      loading={loading}
      renderInput={(params) => (
        <TextField
          autoComplete="off"
          error={!!error}
          helperText={error}
          {...params}
          label={label || undefined}
          placeholder={placeholder}
          fullWidth
          InputProps={{
            ...params.InputProps,
            endAdornment: <>{params.InputProps.endAdornment}</>,
          }}
        />
      )}
    />
  );
}
