import React, { createContext, useState } from "react";
import AlertDialog from "../components/customs/AlertDialog";
import { Stack, useTheme } from "@mui/material";
import { Delete, Warning } from "@mui/icons-material";

export const AlertContext = createContext(null);

export default function AlertProvider({ children }) {
  const [isAlerting, setIsAlerting] = useState(false);
  const [loading, setLoading] = useState(false);
  const initialAlert = {
    icon: null,
    typeDelete: false,
    title: "",
    message: "",
    onCancel: null,
    onConfirm: null,
    form: undefined,
  };
  const [alert, setAlert] = useState(initialAlert);
  const theme = useTheme();

  const promptCloseForm = ({ onSubmit }) => {
    setIsAlerting(true);
    const alertObject = {
      icon: <Warning color="warning" sx={{ fontSize: theme.spacing(10) }} />,
      title: "Do you want to leave this form?",
      message: "Your changes will be lose if you don’t save them.",
      onCancel: () => {
        setIsAlerting(false);
        setTimeout(() => setAlert(initialAlert), 500);
      },
      onConfirm: async () => {
        setLoading(true);
        await onSubmit().then(() => {
          setIsAlerting(false);
          setTimeout(() => setAlert(initialAlert), 500);
        });
        setTimeout(() => setLoading(false), 500);
      },
    };

    setAlert(alertObject);
  };

  const promptDelete = ({ onSubmit }) => {
    setIsAlerting(true);
    const alertObject = {
      icon: <Delete color="error" sx={{ fontSize: theme.spacing(10) }} />,
      title: "Are you sure?",
      message: "You won't be able to recovery it after you delete it.",
      onCancel: () => {
        setIsAlerting(false);
        setTimeout(() => setAlert(initialAlert), 500);
      },
      onConfirm: async () => {
        setLoading(true);
        await onSubmit().then(() => {
          setIsAlerting(false);
          setTimeout(() => setAlert(initialAlert), 500);
        });
        setTimeout(() => setLoading(false), 500);
      },
    };
    setAlert(alertObject);
  };

  const initAlertContext = {
    isAlerting,
    setIsAlerting,
    initialAlert,
    alert,
    setAlert,
    promptCloseForm,
    promptDelete,
  };

  return (
    <AlertContext.Provider value={initAlertContext}>
      {children}
      <AlertDialog
        loading={loading}
        headerComponent={
          alert.icon && (
            <Stack sx={{ justifyContent: "center", alignItems: "center" }}>
              {alert.icon}
            </Stack>
          )
        }
        typeDelete={alert.typeDelete}
        visible={isAlerting}
        title={alert.title}
        textContent={alert.message}
        onCancel={alert.onCancel}
        onConfirm={alert.onConfirm}
        form={alert.form}
        actionComponent={alert.form ? <></> : undefined}
      />
    </AlertContext.Provider>
  );
}
