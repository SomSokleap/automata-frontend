import { Container } from "@mui/material";
import React from "react";
import Page from "src/components/Page";
import { MainContextProvider } from "src/contexts/MainContext";
import Header from "./Header";
import AutomatonList from "./List";

export default function Automata() {
  return (
    <MainContextProvider>
      <Page title="Automata Project">
        <Container maxWidth={false}>
          <Header />
          <AutomatonList />
        </Container>
      </Page>
    </MainContextProvider>
  );
}
