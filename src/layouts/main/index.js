// material
import { Box } from "@mui/material";
import { Helmet } from "react-helmet-async";
import { Outlet } from "react-router-dom";
import Navbar from "./Navbar";

// ----------------------------------------------------------------------

export default function MainLayout() {
  return (
    <>
      <Helmet>
        <style>
          {`
          body::-webkit-scrollbar {
            width: 0px;
          }
          `}
        </style>
      </Helmet>
      <Navbar />
      <Outlet />
    </>
  );
}
