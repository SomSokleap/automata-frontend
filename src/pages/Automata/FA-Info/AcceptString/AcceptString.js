import { LoadingButton } from "@mui/lab";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { set } from "lodash";
import { toast } from "react-toastify";
import { apiUrl } from "src/config";
import axios from "axios";
import { useMainContext } from "src/contexts/MainContext";

export default function AcceptString() {
  const { currentSelected } = useMainContext();
  const [string, setString] = useState();
  const [loading, setLoading] = useState(false);

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const acceptStringHandler = async () => {
    try {
      setLoading(true);
      const body = {
        fa_id: currentSelected.fa_id,
        text: string,
      };
      console.log("Body", body);
      const res = await axios.post(apiUrl + "/api/acceptString", body);

      toast.success(res.data.message);
      setLoading(false);
    } catch (e) {
      toast.error(e.message);
    }
  };

  return (
    <Stack sx={{ mt: 2 }}>
      <Paper sx={{ p: 2 }} elevation={6}>
        {/* <Stack>
          <Typography sx={{ fontWeight: "700", mb: 2 }}>
            Check if the String is Accept of Reject by the Finit Automaton (FA)
          </Typography>
          <FormControl sx={{ width: "100%" }} size="small">
            <InputLabel id="demo-select-small">Age</InputLabel>
            <Select
              fullWidth
              labelId="demo-select-small"
              id="demo-select-small"
              value={age}
              label="Age"
              onChange={handleChange}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </FormControl>
        </Stack> */}

        <Stack>
          <Typography sx={{ fontWeight: "700", mb: 2 }}>
            Input your strings
          </Typography>
          <TextField
            size="small"
            id="outlined-basic"
            label="Strings"
            value={string}
            onChange={(e) => setString(e.target.value)}
            variant="outlined"
          />
        </Stack>

        <Stack
          sx={{ mt: 2 }}
          direction="row"
          justifyContent="flex-end"
          alignItems={"center"}
        >
          <LoadingButton
            sx={{
              backgroundColor: "primary.main",
              "&:hover": {
                backgroundColor: "primary.light",
              },
            }}
            onClick={acceptStringHandler}
            loading={loading}
            startIcon={<PlayArrowIcon />}
            variant="contained"
          >
            Run
          </LoadingButton>
        </Stack>
      </Paper>
    </Stack>
  );
}
