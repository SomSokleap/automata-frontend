import { CropFree, HighlightOff } from "@mui/icons-material";
import {
  Backdrop,
  Box,
  Fade,
  IconButton,
  Modal,
  Paper,
  Typography,
} from "@mui/material";
import { makeStyles, useTheme } from "@mui/styles";
import clsx from "clsx";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import useSettings from "../hooks/useSettings";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  root: {
    transition: "all 0.3s ease !important",
    height: "80%",
    width: "70%",
    maxHeight: "100%",
    maxWidth: "100%",
    display: "flex",
    flexDirection: "column",

    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      width: "85%",
      height: "85%",
    },
    [theme.breakpoints.down("sm")]: {
      width: "90%",
      height: "90%",
    },
    [theme.breakpoints.only("xs")]: {
      width: "100%",
      height: "100%",
    },
    zIndex: 100,
  },
  contentWrapper: {
    flexGrow: 1,
    overflowY: "auto",
  },
  controller: {
    minHeight: 46,
    maxHeight: 46,
    overflow: "hidden",
    display: "flex",
    alignItems: "center",
    width: "100%",
    backgroundColor:
      theme.palette.mode === "light"
        ? theme.palette.grey[300]
        : theme.palette.grey[700],
    padding: theme.spacing(1),
  },
  controllerIcons: {
    display: "flex",
    flexDirection: "flex-end",
    alignItems: "center",
  },
  zoom: {
    width: "100% !important",
    height: "100% !important",
    maxHeight: "100% !important",
    maxWidth: "100% !important",
  },
  elipeseText: {
    display: "box",
    boxOrient: "vertical",
    lineClamp: 1,
    overflow: "hidden",
    wordBreak: "break-all",
    flexGrow: 1,
    textTransform: "uppercase",
    color:
      theme.palette.mode === "light"
        ? theme.palette.grey[600]
        : theme.palette.grey[300],
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

PopupWrapperModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  disableZoom: PropTypes.bool,
  contentStyle: PropTypes.object,
  headerComponent: PropTypes.node,
  alignTitleLeft: PropTypes.bool,
  onClose: PropTypes.func,
  headerLeftIcon: PropTypes.node,
  disableBackDrop: PropTypes.bool,
};

function PopupWrapperModal({
  visible,
  title = "",
  className,
  onClose,
  disableBackDrop = false,
  disableZoom = false,
  contentStyle,
  headerComponent,
  headerLeftIcon,
  alignTitleLeft,
  children,
}) {
  const classes = useStyles();
  const [zoom, setZoom] = useState(false);
  const theme = useTheme();
  const { themeMode } = useSettings();
  const [hideZoom, setHideZoom] = useState(false);

  useEffect(() => {
    if (window.innerWidth <= theme.breakpoints.values.sm) {
      setHideZoom(true);
    }
    window.addEventListener("resize", () => {
      if (window.innerWidth <= theme.breakpoints.values.sm) {
        setHideZoom(true);
      } else {
        setHideZoom(false);
      }
    });
    return () => {
      window.removeEventListener("resize", () => {});
    };
    // eslint-disable-next-line
  }, []);

  return (
    <Modal
      open={visible}
      closeAfterTransition
      className={classes.modal}
      onClose={disableBackDrop ? () => {} : onClose}
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 300,
      }}
    >
      <Fade in={visible}>
        <Paper
          sx={{
            backgroundColor:
              themeMode === "light"
                ? theme.palette.grey[200]
                : theme.palette.grey[800],
          }}
          className={clsx(classes.root, className, { [classes.zoom]: zoom })}
        >
          {headerComponent || (
            <Box className={classes.controller}>
              {/* left icon */}
              {headerLeftIcon}
              {/* End left icon */}

              <Typography
                variant="h6"
                className={classes.elipeseText}
                style={{ textAlign: alignTitleLeft ? "left" : "center" }}
              >
                {title}
              </Typography>

              {/* Right icons */}
              <Box className={classes.controllerIcons}>
                {!hideZoom && (
                  <>
                    {!disableZoom && (
                      <IconButton
                        size="small"
                        onClick={() => setZoom((prev) => !prev)}
                      >
                        <CropFree />
                      </IconButton>
                    )}
                  </>
                )}
                <IconButton size="small" onClick={onClose}>
                  <HighlightOff color="error" />
                </IconButton>
              </Box>
              {/* End right icons */}
            </Box>
          )}
          <Box className={clsx(classes.contentWrapper, contentStyle)}>
            {children}
          </Box>
        </Paper>
      </Fade>
    </Modal>
  );
}

export default PopupWrapperModal;
