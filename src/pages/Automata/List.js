import { Box, Grid, Pagination, Stack } from "@mui/material";
import React from "react";
import EmptyContent from "src/components/EmptyContent";
import LoadingScreen from "src/components/LoadingScreen";
import { useMainContext } from "src/contexts/MainContext";
import AutomatonCard from "./AutomatonCard";

export default function AutomatonList() {
  const { automatas } = useMainContext();
  return (
    <>
      {automatas ? (
        automatas.length > 0 ? (
          <Grid container rowSpacing={2} columnSpacing={2}>
            {automatas?.map((automata, i) => (
              <AutomatonCard data={automata} />
            ))}
          </Grid>
        ) : (
          <EmptyContent title="FA is empty" />
        )
      ) : (
        <Box sx={{ width: "100%", height: "300px" }}>
          <LoadingScreen />
        </Box>
      )}
    </>
  );
}
