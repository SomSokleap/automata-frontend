import React from "react";
import { Stack, Typography, Box } from "@mui/material";
import { ArrowDropDown } from "@mui/icons-material";
import CustomLink from "./CustomLink";

const styles = {
  labelContainer: {
    position: "relative",
    display: "inline-block",
    cursor: "pointer",
  },
  content: {
    transition: "all 0.3s ease-in-out",
    position: "absolute",
    minWidth: "400px",
    maxHeight: "200px",
    boxShadow: "0px 8px 16px 0px rgba(0,0,0,0.2)",
    padding: "12px 16px",
    marginTop: "3px",
    overflow: "auto",
    transitionDuration: "0.4s",
    borderRadius: "3px",
    backgroundColor: "white",
  },
  label: {
    fontWeight: "600",
    fontSize: "13px",
  },
};

const NavLink = ({ label, dropdown, content, url, color }) => {
  const [hover, setHover] = React.useState(false);
  if (dropdown) {
    return (
      <Box
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        sx={styles.labelContainer}
      >
        <Box>
          <Stack direction="row" sx={{ alignItems: "center" }}>
            <Box>
              <CustomLink to={url}>
                <Typography sx={{ ...styles.label, color: color || "white" }}>
                  {label}
                </Typography>
              </CustomLink>

              <Box
                style={{
                  width: hover ? "100%" : 0,
                  height: 2,
                  background: color || "white",
                  transition: "all 0.3s ease-in-out",
                  opacity: hover ? 1 : 0,
                  marginTop: -3,
                }}
              />
            </Box>
            <ArrowDropDown sx={{ color: "white" }} />
          </Stack>
        </Box>
        <Box
          sx={styles.content}
          style={{
            display: hover ? "block" : "none",
          }}
        >
          {content}
        </Box>
      </Box>
    );
  }
  return (
    <CustomLink to={url}>
      <Box
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <Typography sx={{ ...styles.label, color: color || "white" }}>
          {label}
        </Typography>
        <Box
          style={{
            width: hover ? "100%" : 0,
            height: 2,
            background: color || "white",
            marginTop: -3,
            transition: "all 0.3s ease-in-out",
            opacity: hover ? 1 : 0,
          }}
        />
      </Box>
    </CustomLink>
  );
};

export default NavLink;
