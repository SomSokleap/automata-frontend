import * as React from "react";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";

export default function ChartMenu(prop) {
  const { title, content, isIcon } = prop;
  return (
    <PopupState variant="popover" popupId="popup-menu">
      {(popupState) => (
        <>
          {isIcon ? (
            <Box sx={{ height: "auto" }} {...bindTrigger(popupState)}>
              {title}
            </Box>
          ) : (
            <Button
              sx={{ boxShadow: 1 }}
              variant="contained"
              {...bindTrigger(popupState)}
            >
              {title}
            </Button>
          )}
          <Menu {...bindMenu(popupState)}>
            {content.map((item, index) => (
              <MenuItem
                key={index}
                onClick={() => {
                  item.onClick();
                  popupState.close();
                }}
              >
                {item.title}
              </MenuItem>
            ))}
          </Menu>
        </>
      )}
    </PopupState>
  );
}
