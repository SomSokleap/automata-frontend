import React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import useMediaQuery from "@mui/material/useMediaQuery";
import SwiperCore, { Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react/swiper-react";
import "swiper/swiper-bundle.css";
import "swiper/swiper.min.css";

// install Swiper modules
SwiperCore.use([Pagination]);

function SwipeableWrapper({ data, card }) {
  const xs = useMediaQuery((theme) => theme.breakpoints.down("xs"));
  const sm = useMediaQuery((theme) => theme.breakpoints.down("sm"));
  const md = useMediaQuery((theme) => theme.breakpoints.down("md"));
  const lg = useMediaQuery((theme) => theme.breakpoints.down("lg"));
  const xl = useMediaQuery((theme) => theme.breakpoints.down("xl"));

  function getSlidePerView() {
    if (xs) {
      return 1;
    }
    if (sm) {
      return 1;
    }
    if (md) {
      return 2;
    }
    if (lg) {
      return 3;
    }
    if (xl) {
      return 3;
    }
    return 4;
  }

  return (
    <Swiper
      // slidesPerView={getSlidePerView()}
      spaceBetween={0}
      pagination={{
        clickable: true,
      }}
    >
      {data.map((item, i) => (
        <SwiperSlide key={i}>
          {card(item)}
          <Box sx={{ height: 50 }} />
        </SwiperSlide>
      ))}
    </Swiper>
  );
}

SwipeableWrapper.propTypes = {
  card: PropTypes.func,
  data: PropTypes.array,
};

export default SwipeableWrapper;
