import { Box } from "@mui/material";
import React from "react";
import Header from "./Header";

export default function MainLayout() {
  return (
    <Box>
      <Header />
    </Box>
  );
}
