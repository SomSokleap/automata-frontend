// * MUI Import
import * as React from "react";
import Box from "@mui/material/Box";
import Icon from "@mui/material/Icon";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import PersonAdd from "@mui/icons-material/PersonAdd";
import Settings from "@mui/icons-material/Settings";
import Logout from "@mui/icons-material/Logout";
import { PeopleOutlineRounded } from "@mui/icons-material";

// * Other Import
import {
  logout,
  getCurrentUser,
  getCurrentRole,
  setCurrentRole,
} from "../../utils/Service";
import { PRIMARY } from "../../theme/palette";

export default function RoleMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const user = getCurrentUser();
  const currentRole = getCurrentRole();
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <>
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Tooltip title="Manage Roles">
          <IconButton onClick={handleClick} size="medium" sx={{ ml: 2 }}>
            <Icon>manage_accounts</Icon>
          </IconButton>
        </Tooltip>
      </Box>
      {user && (
        <Menu
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          onClick={handleClose}
          PaperProps={{
            elevation: 0,
            sx: {
              overflow: "visible",
              filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
              mt: 1.5,
              "& .MuiAvatar-root": {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
              },
              "&:before": {
                content: '""',
                display: "block",
                position: "absolute",
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                bgcolor: "background.paper",
                transform: "translateY(-50%) rotate(45deg)",
                zIndex: 0,
              },
            },
          }}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        >
          <Typography sx={{ px: 3 }}>Current Role:</Typography>
          <Divider />
          {user.user_data.get_all_roles.map((role, i) => (
            <MenuItem
              onClick={() => setCurrentRole(role)}
              sx={{
                color: currentRole.id === role.id ? PRIMARY.main : "inherit",
              }}
            >
              {role.name}
            </MenuItem>
          ))}
        </Menu>
      )}
    </>
  );
}
