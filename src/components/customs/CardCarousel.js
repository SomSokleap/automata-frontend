import { makeStyles } from "@mui/styles";
import React from "react";
import Carousel from "react-elastic-carousel";
import { PRIMARY } from "../../theme/palette";

const useStyles = makeStyles({
  root: {
    "& .rec.rec-arrow": {
      backgroundColor: "whitesmoke",
    },
    "& .rec.rec-arrow:hover": {
      backgroundColor: PRIMARY.main,
    },
  },
});

function CardCarousel(prop) {
  const { children } = prop;
  const classes = useStyles();
  const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 1, itemsToScroll: 2, pagination: false },
    { width: 850, itemsToShow: 2 },
    { width: 1150, itemsToShow: 2, itemsToScroll: 2 },
    { width: 1450, itemsToShow: 2 },
    { width: 1750, itemsToShow: 2 },
  ];

  return (
    <Carousel
      pagination={false}
      itemsToShow={2}
      breakPoints={breakPoints}
      className={classes.root}
    >
      {children}
    </Carousel>
  );
}

export default CardCarousel;
