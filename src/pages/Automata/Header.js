import { Button, Stack, Typography } from "@mui/material";
import LibraryAddIcon from "@mui/icons-material/LibraryAdd";
import React, { useState } from "react";
import PopupWrapperModal from "src/components/PopupWrapperModal";
import Create from "./Modals/Create";
import { useMainContext } from "src/contexts/MainContext";
import FAInfo from "./FA-Info";

const styles = {
  root: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    flexWrap: "wrap",
    mt: 2,
    mb: 3,
    mx: 2,
  },
  AddButton: {
    backgroundColor: "primary.main",
    "&:hover": {
      backgroundColor: "primary.light",
    },
    color: "white",
  },
};

export default function Header() {
  const { currentSelected, setCurrentSelected } = useMainContext();
  const [isCreating, setIsCreating] = useState();
  return (
    <>
      <Stack sx={styles.root}>
        <Typography sx={{ fontWeight: "bold", fontSize: "20px" }}>
          Automaton Management
        </Typography>
        <Button
          variant="contained"
          startIcon={<LibraryAddIcon />}
          onClick={() => setIsCreating(true)}
          sx={styles.AddButton}
        >
          Desgin FA
        </Button>
      </Stack>

      {/* Create Modal */}
      <PopupWrapperModal
        visible={isCreating}
        onClose={() => {
          setIsCreating();
        }}
        title="Desgin FA"
      >
        <Create />
      </PopupWrapperModal>

      {/* Check Modal */}
      {currentSelected && (
        <PopupWrapperModal
          visible={!!currentSelected}
          onClose={() => {
            setCurrentSelected(undefined);
          }}
          title="Automaton Detail"
        >
          <FAInfo />
        </PopupWrapperModal>
      )}
    </>
  );
}
