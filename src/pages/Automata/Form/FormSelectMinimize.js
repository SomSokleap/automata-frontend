import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Typography } from '@mui/material';
import { Button, Grid, Stack } from '@mui/material';
import { Backup } from '@mui/icons-material';

export default function FormSelectMinimize () {
    const [fa, setFA] = React.useState('');

    const handleChange = (event) => {
      setFA(event.target.value);
    };
  
    return (
      <Box sx={{ minWidth: 120 }}>
        <Typography sx={{ pb: 2 }} variant="h6">Minimize the Deterministic Finite Automaton (DFA)</Typography>
        <FormControl fullWidth>
          <InputLabel>Finite Automata (FA)</InputLabel>
          <Select
            value={fa}
            label="Finite Automata (FA)"
            onChange={handleChange}
          >
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
        <Stack sx={{ pt: 2 }} spacing={2} direction='row'>
        <Grid container justifyContent='flex-end'>
          <Button type='submit' variant='contained' startIcon={<Backup />}>
            Submit
          </Button>
        </Grid>
      </Stack>
      </Box>
    );
}

