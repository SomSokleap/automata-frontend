import { Box, Container, Stack, Typography } from "@mui/material";
import React from "react";

const styles = {
  root: {
    display: {
      xs: "none",
      sm: "block",
    },
    width: "100%",
    transition: "background 300ms ease-in-out",
    backgroundColor: "primary.main",
  },
  content: {
    height: 50,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  TitleHeader: {
    color: "white",
    fontWeight: 600,
    fontSize: "20px",
  },
};

export default function Header() {
  return (
    <Box sx={styles.root}>
      <Container maxWidth="xl" sx={styles.content}>
        <Stack
          direction="row"
          spacing={1}
          alignItems="center"
          justifyContent="center"
          width="100%"
        >
          <Typography sx={styles.TitleHeader}>Automata | Group 07</Typography>
        </Stack>
      </Container>
    </Box>
  );
}
