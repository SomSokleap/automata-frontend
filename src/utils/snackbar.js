import { toast } from "react-toastify";

const snackbarOptions = {
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};

const customSnackbar = {
  success: (msg) => {
    toast.success(msg, snackbarOptions);
  },
  error: (msg) => {
    toast.error(msg, snackbarOptions);
  },
};

export default customSnackbar;
