import React from "react";
import "react-quill/dist/quill.snow.css";
import ReactQuill from "react-quill";

export default function Quill({ simple, ...rest }) {
  return <ReactQuill theme={"snow"} style={{ height: "250px" }} {...rest} />;
}
