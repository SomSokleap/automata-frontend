import InsertInvitationIcon from "@mui/icons-material/InsertInvitation";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import CalendarPickerSkeleton from "@mui/lab/CalendarPickerSkeleton";
import DatePicker from "@mui/lab/DatePicker";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import PickersDay from "@mui/lab/PickersDay";
import FormControl from "@mui/material/FormControl";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import TextField from "@mui/material/TextField";
import { capitalize } from "lodash";
import * as React from "react";

// import { authRequest } from '../../Service';

export default function ServerRequestDatePicker({
  label,
  value,
  setValue,
  disabled,
  nullable,
  small,
}) {
  const requestAbortController = new AbortController();
  const [isLoading, setIsLoading] = React.useState(true);
  const [highlightedDays, setHighlightedDays] = React.useState([]);

  const fetchHighlightedDays = async () => {
    try {
      // Fetch data by sending (date) as a
      // ...
      // const res = await authRequest.post('/getHighlightedDateAPI', { date });
      const resData = [1, 2, 3];
      setHighlightedDays([null, ...resData]);
      setTimeout(() => setIsLoading(false), 1000);
    } catch (error) {
      setIsLoading(false);
    }
  };

  React.useEffect(() => {
    setIsLoading(true);
    fetchHighlightedDays(new Date());
    // eslint-disable-next-line
    return () => requestAbortController.current?.abort();
    // eslint-disable-next-line
  }, []);

  const handleMonthChange = (date) => {
    if (requestAbortController.current) {
      // make sure that you are aborting useless requests
      // because it is possible to switch between months pretty quickly
      requestAbortController.current.abort();
    }
    setIsLoading(true);
    setHighlightedDays([]);
    fetchHighlightedDays(date);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      {nullable && value === null ? (
        <FormControl sx={{ m: 1, flex: 1 }} variant="outlined">
          <InputLabel htmlFor={label}>{capitalize(label)}</InputLabel>
          <OutlinedInput
            size={small ? "small" : "medium"}
            disabled
            id={label}
            value="---"
            endAdornment={
              <InputAdornment position="end">
                <IconButton edge="end" disabled>
                  <InsertInvitationIcon />
                </IconButton>
              </InputAdornment>
            }
            label={label || undefined}
          />
        </FormControl>
      ) : (
        <DatePicker
          disabled={disabled}
          label={label || undefined}
          inputFormat="dd/MM/yyyy"
          value={value}
          loading={isLoading}
          onChange={(newValue) => {
            setValue(newValue);
          }}
          onMonthChange={handleMonthChange}
          renderInput={(params) => (
            <TextField
              size={small ? "small" : "medium"}
              sx={{ flex: 1 }}
              {...params}
            />
          )}
          renderLoading={() => <CalendarPickerSkeleton />}
          renderDay={(day, _value, DayComponentProps) => {
            const isSelected =
              !DayComponentProps.outsideCurrentMonth &&
              highlightedDays.indexOf(day.getDate()) > 0;

            return (
              <PickersDay
                {...DayComponentProps}
                sx={{
                  borderRadius: "100%",
                  border: isSelected ? "1px red solid" : "none",
                  color: isSelected ? "red" : "text.primary",
                  pointerEvents: isSelected ? "none" : "auto",
                }}
              />
            );
          }}
        />
      )}
    </LocalizationProvider>
  );
}
