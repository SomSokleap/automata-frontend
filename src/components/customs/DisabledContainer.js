import React from "react";

import { Box } from "@mui/material";

export default function DisabledContainer({ children }) {
  return <Box sx={{ pointerEvents: "none" }}>{children}</Box>;
}
